
/**
 * WsM6MarcarElementoServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsM6MarcarElementoServiceSkeleton java skeleton for the axisService
     */
    public class WsM6MarcarElementoServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsM6MarcarElementoServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param fechaSolicitud
                                     * @param numeroTiquete
                                     * @param idElemento
                                     * @param elementoMarcado
                                     * @param tipoMarcacion
                                     * @param detalleRespuesta
                                     * @param notas
                                     * @param atributos
         */
        

                 public co.net.une.www.ncainvm6.M6MarcarElementoType M6MarcarElementoOperation
                  (
                  co.net.une.www.ncainvm6.UTCDate fechaSolicitud,java.lang.String numeroTiquete,java.lang.String idElemento,java.lang.String elementoMarcado,java.lang.String tipoMarcacion,co.net.une.www.ncainvm6.Detallerespuestatype detalleRespuesta,co.net.une.www.ncainvm6.Listanotastype notas,co.net.une.www.ncainvm6.Listaatributostype atributos
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("fechaSolicitud",fechaSolicitud);params.put("numeroTiquete",numeroTiquete);params.put("idElemento",idElemento);params.put("elementoMarcado",elementoMarcado);params.put("tipoMarcacion",tipoMarcacion);params.put("detalleRespuesta",detalleRespuesta);params.put("notas",notas);params.put("atributos",atributos);
		try{
		
			return (co.net.une.www.ncainvm6.M6MarcarElementoType)
			this.makeStructuredRequest(serviceName, "M6MarcarElementoOperation", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    