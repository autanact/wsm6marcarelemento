
/**
 * WsM6MarcarElementoServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WsM6MarcarElementoServiceMessageReceiverInOut message receiver
        */

        public class WsM6MarcarElementoServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WsM6MarcarElementoServiceSkeleton skel = (WsM6MarcarElementoServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("M6MarcarElementoOperation".equals(methodName)){
                
                co.net.une.www.ncainvm6.M6MarcarElemento m6MarcarElemento10 = null;
	                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedParam =
                                                             (co.net.une.www.ncainvm6.M6MarcarElemento)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.ncainvm6.M6MarcarElemento.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               m6MarcarElemento10 =
                                                   
                                                   
                                                           wrapM6MarcarElementoOperation(
                                                       
                                                        

                                                        
                                                       skel.M6MarcarElementoOperation(
                                                            
                                                                getFechaSolicitud(wrappedParam)
                                                            ,
                                                                getNumeroTiquete(wrappedParam)
                                                            ,
                                                                getIdElemento(wrappedParam)
                                                            ,
                                                                getElementoMarcado(wrappedParam)
                                                            ,
                                                                getTipoMarcacion(wrappedParam)
                                                            ,
                                                                getDetalleRespuesta(wrappedParam)
                                                            ,
                                                                getNotas(wrappedParam)
                                                            ,
                                                                getAtributos(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), m6MarcarElemento10, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.ncainvm6.M6MarcarElemento param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.ncainvm6.M6MarcarElemento.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.ncainvm6.M6MarcarElemento param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.ncainvm6.M6MarcarElemento.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.ncainvm6.UTCDate getFechaSolicitud(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getFechaSolicitud();
                            
                        }
                     

                        private java.lang.String getNumeroTiquete(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getNumeroTiquete();
                            
                        }
                     

                        private java.lang.String getIdElemento(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getIdElemento();
                            
                        }
                     

                        private java.lang.String getElementoMarcado(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getElementoMarcado();
                            
                        }
                     

                        private java.lang.String getTipoMarcacion(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getTipoMarcacion();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Detallerespuestatype getDetalleRespuesta(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getDetalleRespuesta();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listanotastype getNotas(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getNotas();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listaatributostype getAtributos(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                        
                                return wrappedType.getM6MarcarElemento().getAtributos();
                            
                        }
                     
                        private co.net.une.www.ncainvm6.M6MarcarElementoType getM6MarcarElementoOperation(
                        co.net.une.www.ncainvm6.M6MarcarElemento wrappedType){
                            return wrappedType.getM6MarcarElemento();
                        }
                        
                        
                    
                         private co.net.une.www.ncainvm6.M6MarcarElemento wrapM6MarcarElementoOperation(
                            co.net.une.www.ncainvm6.M6MarcarElementoType innerType){
                                co.net.une.www.ncainvm6.M6MarcarElemento wrappedElement = new co.net.une.www.ncainvm6.M6MarcarElemento();
                                wrappedElement.setM6MarcarElemento(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.ncainvm6.M6MarcarElemento.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.M6MarcarElemento.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.ncainvm6.M6MarcarElemento.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.M6MarcarElemento.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    